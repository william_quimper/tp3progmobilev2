package com.example.a23_tp3_depart.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.NavDirections
import androidx.navigation.Navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.a23_tp3_depart.R
import com.example.a23_tp3_depart.model.Locat

class HomeAdapter : RecyclerView.Adapter<HomeAdapter.NoteHolder>() {
    // Liste des emplacements à afficher
    private var locations: List<Locat> = ArrayList<Locat>()

    // Contexte de l'application
    private var context: Context? = null

    // Crée et retourne un objet NoteHolder qui contient la vue de chaque élément de la liste
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteHolder {
        val itemView: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.location_row, parent, false)
        context = parent.context
        return NoteHolder(itemView)
    }

    // Associe les données de l'emplacement actuel à la vue NoteHolder
    override fun onBindViewHolder(holder: NoteHolder, position: Int) {
        val currentLocation: Locat = locations[position]
        val categorie: String = currentLocation.categorie
        val itemId: Int = currentLocation.id!!

        // Affiche les détails de l'emplacement dans la vue
        holder.tvCategorie.text = categorie
        holder.tvNom.text = currentLocation.nom
        holder.tvAdresse.text = currentLocation.adresse
        holder.ivLocation.setImageResource(R.drawable.pleinair)

        // Sélectionne l'image en fonction de la catégorie
        when (categorie) {
            "Sport" -> holder.ivLocation.setImageResource(R.drawable.sports)
            "Camping" -> holder.ivLocation.setImageResource(R.drawable.camping)
            "Voyage" -> holder.ivLocation.setImageResource(R.drawable.voyage)
        }

        // Définit le gestionnaire de clic sur l'élément de la liste
        holder.itemView.setOnClickListener { v ->
            // Crée une action de navigation vers le fragment de détails avec l'ID de l'emplacement
            val action = HomeFragmentDirections.actionNavHomeToNavDetails(itemId)
            // Navigue vers le fragment de détails
            findNavController(v).navigate(action)
        }
    }

    // Retourne le nombre total d'éléments dans la liste
    override fun getItemCount(): Int {
        return locations.size
    }

    // Met à jour la liste des emplacements et notifie l'adaptateur du changement
    fun setLocations(locations: List<Locat>) {
        this.locations = locations
        notifyDataSetChanged()
    }

    // Retourne la liste actuelle des emplacements
    fun getLocations(): List<Locat> {
        return locations
    }

    inner class NoteHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvCategorie: TextView
        val tvNom: TextView
        val tvAdresse: TextView
        val ivLocation: ImageView

        // Initialise les vues
        init {
            tvCategorie = itemView.findViewById<TextView>(R.id.tv_categorie)
            tvNom = itemView.findViewById<TextView>(R.id.tv_nom)
            tvAdresse = itemView.findViewById<TextView>(R.id.tv_adresse)
            ivLocation = itemView.findViewById<ImageView>(R.id.iv_loc_cat)
        }
    }
}
