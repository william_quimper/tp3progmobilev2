package com.example.a23_tp3_depart.ui.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.a23_tp3_depart.R
import com.example.a23_tp3_depart.databinding.FragmentDetailsBinding
import com.example.a23_tp3_depart.model.Locat
import com.example.a23_tp3_depart.ui.home.HomeViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions


class DetailsFragment : Fragment(), OnMapReadyCallback {

    private var _binding: FragmentDetailsBinding? = null

    private val binding get() = _binding!!

    private lateinit var mMap: GoogleMap

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val galleryViewModel =
            ViewModelProvider(this).get(DetailsViewModel::class.java)

        _binding = FragmentDetailsBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val mapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)


        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireActivity().title = "Details"

        assert(arguments != null)
        val itemId = arguments?.getInt("id")

        val detailViewModel = ViewModelProvider(this)[DetailsViewModel::class.java]
        detailViewModel.setSelectedLocationId(itemId!!)

        // Retrieve values from TextViews
        val nomDetailsTextView = binding.tvNomDetails
        val idTextView = binding.tvId
        val categorieDetailsTextView = binding.tvCategorieDetails
        val adresseDetailsTextView = binding.tvAdresseDetails

        // obtiens la location clické
        detailViewModel.getSelectedLocation().observe(viewLifecycleOwner) { location ->
            if (location != null) {
                // Attribue les informations de la location clické
                nomDetailsTextView.text = location.nom
                idTextView.text = "# ${location.id}"
                categorieDetailsTextView.text = location.categorie
                adresseDetailsTextView.text = location.adresse

                // set l'image selon la catégorie
                val imageResourceId = getImageResourceId(location.categorie)
                binding.ivLocationBottom.setImageResource(imageResourceId)

                // mets un marker sur la map
                updateGoogleMap(location.latitude, location.longitude)

            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    /**
     * Set l'image selon la catégorie
     */
    private fun getImageResourceId(category: String): Int {
        return when (category) {
            "Sport" -> R.drawable.sports
            "Camping" -> R.drawable.camping
            "Voyage" -> R.drawable.voyage
            else -> R.drawable.pleinair // Provide a default image
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

    }

    /**
     * Mets un marker sur la map et zoom à sa position
     */
    private fun updateGoogleMap(latitude: Double, longitude: Double) {
        val selectedLocation = LatLng(latitude, longitude)

        // Ajouter un marker sur la map
        mMap.addMarker(MarkerOptions().position(selectedLocation).title("Selected Location"))

        // Déplacer la caméra a la position du marker
        val cameraPosition = CameraPosition.Builder()
            .target(selectedLocation)
            .zoom(3f)
            .build()
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }
}