package com.example.a23_tp3_depart.ui.map

import android.Manifest
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.BitmapFactory
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.ImageView
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.UiThread
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.a23_tp3_depart.R
import com.example.a23_tp3_depart.databinding.FragmentMapBinding
import com.example.a23_tp3_depart.model.Locat
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.floatingactionbutton.FloatingActionButton
import okhttp3.Call
import okhttp3.Callback
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.json.JSONObject
import java.io.IOException
import java.io.InputStream
import kotlin.concurrent.thread


class MapFragment : Fragment(), OnMapReadyCallback {

    private var _binding: FragmentMapBinding? = null

    val LOCATION_PERMISSION_CODE = 1

    private val binding get() = _binding!!

    private lateinit var mMap: GoogleMap

    // Position de l'utilisateur
    lateinit var userLocation: Location

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private lateinit var locationRequest: LocationRequest

    private lateinit var fab: FloatingActionButton

    private var fonctionAjoutActive = false




    // Déclaration pour le callback de la mise à jour de la position de l'utilisateur
    // Le callback est appelé à chaque fois que la position de l'utilisateur change
    private var locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            super.onLocationResult(locationResult)
            userLocation = locationResult.lastLocation!!
            Log.d(
                "***POSITION***",
                "onLocationResult: ${userLocation?.latitude} ${userLocation?.longitude}"
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        val mapViewModel = ViewModelProvider(this)[MapViewModel::class.java]

        _binding = FragmentMapBinding.inflate(inflater, container, false)
        val root: View = binding.root

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mapFragment =
            childFragmentManager.findFragmentById(com.example.a23_tp3_depart.R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)


        // Click sur le fab
        fab = view.findViewById(R.id.fab)
        fab.setOnClickListener {
            fonctionAjoutActive = !fonctionAjoutActive

            // Set la couleur du fab
            if (!fonctionAjoutActive) {
                val color1 = ContextCompat.getColor(requireContext(), R.color.fab_couleur_initiale);
                fab.backgroundTintList = ColorStateList.valueOf(color1);
            } else {
                val color2 =
                    ContextCompat.getColor(requireContext(), R.color.fab_couleur_secondaire);
                fab.backgroundTintList = ColorStateList.valueOf(color2);
            }

        }
    }

    /**
     * Cette méthode est appelée lorsque l'utilisateur répond à la demande de permission.
     * @param requestCode Le code de la demande de permission
     * @param permissions La liste des permissions demandées
     * @param grantResults La liste des réponses de l'utilisateur pour chaque permission demandée (granted ou denied)
     */
    @Suppress("DEPRECATION")
    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>, grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            LOCATION_PERMISSION_CODE -> {
                // Si la demande est annulée, les tableaux de résultats (grantResults) sont vides.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // Si la permission est refusée, on affiche un message d'information
                } else if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    // Le système décide s'il faut afficher une explication supplémentaire
                    // À nouveau, vérification d'un requis de permission pour cette app
                    if (ActivityCompat.shouldShowRequestPermissionRationale(
                            requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION
                        )
                    ) {
                        // On affiche une boîte de dialogue pour expliquer pourquoi la permission est requise
                        val dialog = AlertDialog.Builder(requireActivity())
                        dialog.setTitle("Permission requise !")
                        dialog.setMessage("Cette permission est importante pour la géolocalisation...")
                        dialog.setPositiveButton("Ok") { dialog, which ->
                            // On demande à nouveau la permission
                            ActivityCompat.requestPermissions(
                                requireActivity(),
                                arrayOf(Manifest.permission.SEND_SMS),
                                LOCATION_PERMISSION_CODE
                            )
                        }
                        dialog.setNegativeButton("Annuler") { dialog, which ->
                            Toast.makeText(
                                requireActivity(),
                                "Pas de géolocalisation possible sans permission",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        dialog.show()
                    }
                }
            }
        }
    }

    /**
     * Permet d'activer la localisation de l'utilisateur
     */
    private fun enableMyLocation() {
        // vérification si la permission de localisation est déjà donnée
        if (ContextCompat.checkSelfPermission(
                requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            if (mMap != null) {
                // Permet d'afficher le bouton pour centrer la carte sur la position de l'utilisateur
                mMap.isMyLocationEnabled = true
            }
        } else {
            // La permission est manquante, demande donc la permission
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_CODE
            )
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onMapReady(googleMap: GoogleMap) {

        mMap = googleMap

        mMap.setOnMarkerClickListener {

            val color1 = ContextCompat.getColor(requireContext(), R.color.fab_couleur_initiale)
            fab.backgroundTintList = ColorStateList.valueOf(color1)

            fonctionAjoutActive = false

            // Return false to allow default marker click behavior
            false
        }

        // Active la localisation de l'utilisateur
        enableMyLocation()

        // Click sur la carte
        // Ajoute un marker si le mode d'ajout est activé
        mMap.setOnMapClickListener { point ->
            if (fonctionAjoutActive) {
                // If addingPointMode is true, show the form for adding a point
                showAddPointForm(point)
            }
        }

        // Active les boutons Zoom in, Zoom out
        val uiSettings = googleMap.uiSettings
        uiSettings.isZoomControlsEnabled = true

        // Configuration du Layout pour les popups (InfoWindow)
        mMap.setInfoWindowAdapter(object : InfoWindowAdapter {
            override fun getInfoWindow(marker: Marker): View? {
                return null
            }

            // Obtiens les informations du marker
            override fun getInfoContents(marker: Marker): View {
                val view: View = LayoutInflater.from(requireActivity())
                    .inflate(com.example.a23_tp3_depart.R.layout.info_window_layout, null)
                updateInfoWindowView(view, marker)

                return view
            }
        })


        // Vérifie les permissions avant d'utiliser le service fusedLocationClient.getLastLocation()
        // qui permet de connaître la dernière position
        if (ActivityCompat.checkSelfPermission(
                requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireActivity(), Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // Demande la permission à l'utilisateur
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_CODE
            )
            // Si la permission n'est pas accordée, on ne va pas plus loin
            return
        }

        /**
         * Localisation
         */
        fusedLocationClient.lastLocation.addOnSuccessListener(requireActivity()) { location ->
                // Vérifie que la position n'est pas null
                if (location != null) {
                    Log.d("***POSITION***", "onSuccess: $location")
                    // Centre la carte sur la position de l'utilisateur au démarrage
                    val latLng = LatLng(location.latitude, location.longitude)
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 5f))

                    // Get user location before getting all markers
                    // Assign the user location
                    userLocation = location

                    getAllMarkers()
                }
            }

        // Configuration pour mise à jour automatique de la position
        locationRequest = LocationRequest.Builder(Priority.PRIORITY_HIGH_ACCURACY, 5000L)
            .setWaitForAccurateLocation(false).setMinUpdateIntervalMillis(2000L)
            .setMaxUpdateDelayMillis(5000L).build()

        // Création de la requête pour la mise à jour de la position
        // avec la configuration précédente
        val request = LocationSettingsRequest.Builder().addLocationRequest(locationRequest).build()

        // Création du client pour la mise à jour de la position.
        // Le client va permettre de vérifier si la configuration est correcte,
        // si l'utilisateur a activé ou désactivé la localisation
        val client = LocationServices.getSettingsClient(requireActivity())

        // Vérifie que la configuration de la mise à jour de la position est correcte
        // Si l'utilisateur a activé ou désactivé la localisation
        client.checkLocationSettings(request).addOnSuccessListener {
                Log.d("***POSITION***", "onSuccess: $it")
                // Si la configuration est correcte, on lance la mise à jour de la position
                fusedLocationClient.requestLocationUpdates(
                    // Configuration de la mise à jour de la position
                    locationRequest,
                    // Callback pour la mise à jour de la position
                    locationCallback, null
                )
            }.addOnFailureListener {
                Log.d("***POSITION***", "onFailure: $it")
                // Si la configuration n'est pas correcte, on affiche un message
                Toast.makeText(
                    requireActivity(), "Veuillez activer la localisation", Toast.LENGTH_SHORT
                ).show()
            }
        /**
         * Fin Localisation
         */
    }

    /**
     * Informations affichés lorsqu'il y a un click sur un marker
     */
    private fun updateInfoWindowView(view: View, marker: Marker) {
        // Extraire les informations du marqueur
        val imageViewCategory: ImageView = view.findViewById(R.id.imageViewCategory)
        val textViewName: TextView = view.findViewById(R.id.textViewName)
        val textViewSnippet: TextView = view.findViewById(R.id.textViewSippet)

        val markerTitle = marker.title
        val markerSnippet = marker.snippet

        textViewName.text = "Nom: $markerTitle"
        textViewSnippet.text = "$markerSnippet"

        // Split the snippet into lines
        val lines = markerSnippet?.split("\n")

        // Find the line that contains "Categorie"
        val categoryLine = lines?.find { it.startsWith("Categorie") }

        // Extract the category from the line
        val category = categoryLine?.substringAfter("Categorie: ") ?: ""

        var iconResId = R.drawable.pleinair // Default icon resource ID

        when (category) {
            "Sport" -> imageViewCategory.setImageResource(R.drawable.sports)
            "Camping" -> imageViewCategory.setImageResource(R.drawable.camping)
            "Voyage" -> imageViewCategory.setImageResource(R.drawable.voyage)
        }
    }

    /**
     * Crée une boite dialogue AlertDialogue avec
     * un formulaire contenant un Spinner & un EditText.
     * Ajoute un marker sur la map si les informations fournies sont
     * valides.
     */
    private fun showAddPointForm(point: LatLng) {
        // Créer une boîte de dialogue AlertDialog
        val dialogBuilder = AlertDialog.Builder(requireContext())

        // Gonfler la mise en page personnalisée pour le formulaire
        val inflater = layoutInflater
        val dialogView = inflater.inflate(R.layout.custom_form_layout, null)

        // Obtenir des références aux éléments d'interface utilisateur dans la mise en page personnalisée
        val editTextName: EditText = dialogView.findViewById(R.id.editTextName)
        val spinnerCategory: Spinner = dialogView.findViewById(R.id.spinnerCategory)

        // Définir les catégories pour le spinner
        val categories = arrayOf("Sport", "Camping", "Voyage")

        // Créer un ArrayAdapter pour le spinner
        val adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, categories)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        // Définir l'adaptateur pour le spinner
        spinnerCategory.adapter = adapter

        // Définir la vue gonflée pour le constructeur de la boîte de dialogue
        dialogBuilder.setView(dialogView)

        // Définir le bouton positif (Soumettre)
        dialogBuilder.setPositiveButton("Soumettre") { _, _ ->
            // Récupérer les données du formulaire
            val name = editTextName.text.toString().trim()
            val category = spinnerCategory.selectedItem.toString().trim()

            // Valide les champs
            if (name.isNotEmpty() && category.isNotEmpty()) {

                val distance = calculateDistance(point)

                // Les champs sont valides, appeler une fonction pour ajouter un marqueur sur la carte avec les données fournies
                if (distance != null) {
                    addMarkerOnMap(name, category, point, distance)
                }

                // Ajoute les informations du marker à la BD
                insertLocationIntoDatabase(name, category, point)


            } else {
                // Afficher un message d'erreur ou effectuer une autre action en cas de champs invalides
                Toast.makeText(
                    requireContext(), "Veuillez remplir tous les champs", Toast.LENGTH_SHORT
                ).show()
            }
        }

        // Définir le bouton négatif (Annuler)
        dialogBuilder.setNegativeButton("Annuler") { dialog, _ ->

            // Rejeter la boîte de dialogue si Annuler est cliqué
            dialog.dismiss()
        }

        // Créer et afficher la boîte de dialogue AlertDialog
        val alertDialog = dialogBuilder.create()
        alertDialog.show()
    }

    /**
     * Crée un objet MarkerOptions afin de l'afficher sur la map
     */
    private fun addMarkerOnMap(name: String, category: String, point: LatLng, distance: Float) {
        getAddressForLocation(point.latitude, point.longitude) { formattedAddress ->

            // Créer le snippet
            val snippet = "Categorie: $category\nDistance: $distance mètres\nAdresse: $formattedAddress"

            // Créer le Marker
            val markerOptions = MarkerOptions().position(point)
                .title(name)
                .snippet(snippet)

            // Ajouter le marker a la map
            mMap.addMarker(markerOptions)
        }

        // Note: You cannot use formattedAddress here as it is not available outside the callback.
    }

    /**
     * Insert la donnée Locat (nom, categorie, adresse, latitude et longitude)
     * L'addresse est trouvée grâce à l'API Google
     * Le nom et la catégorie sont inscrits par le user
     * La latitude et la longitude sont l'emplacement du marker
     */
    private fun insertLocationIntoDatabase(name: String, category: String, point: LatLng) {
        val viewModel = ViewModelProvider(requireActivity())[MapViewModel::class.java]
        viewModel.setContext(requireContext())

        getAddressForLocation(point.latitude, point.longitude) { formattedAddress ->
            val locat = Locat(name, category, formattedAddress, point.latitude, point.longitude)
            // insère la localisation a la bd
            thread{
                viewModel.insertLocation(locat)
            }
        }
    }


    /**
     * Calcule la distance entre la localisation du user
     * et celle du marker
     */
    private fun calculateDistance(point: LatLng): Float? {
        return userLocation?.distanceTo(Location("Point").apply {
            latitude = point.latitude
            longitude = point.longitude
        })
    }

    /**
     * Obtiens tous les markers de la base de donnée et les place sur la map
     */
    private fun getAllMarkers(){
        val mapViewModel = ViewModelProvider(this)[MapViewModel::class.java]
        mapViewModel.setContext(context)

        mapViewModel.getAllLocations().observe(viewLifecycleOwner) { points ->
            mMap.clear()

            // Ajouter tous les markers de la liste
            points.forEach { point ->
                val pointDistance = calculateDistance(LatLng(point.latitude, point.longitude))
                if (pointDistance != null) {
                    addMarkerOnMap(
                        point.nom,
                        point.categorie,
                        LatLng(point.latitude, point.longitude),
                        pointDistance
                    )
                }
            }
        }
    }

    /**
     * Obtiens l'adresse selon la longitude et la latitude
     */
    private fun getAddressForLocation(latitude: Double, longitude: Double, callback: (String) -> Unit) {
        val handler = Handler(Looper.getMainLooper())

        val urlAPI =
            "https://maps.googleapis.com/maps/api/geocode/json?latlng=$latitude,$longitude&key=%20AIzaSyBtl5m9DuFa9CRo8-iGDX00xYh40AFXoas"
        val client = OkHttpClient()
        val request = Request.Builder()
            .url(urlAPI)
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            override fun onResponse(call: Call, response: Response) {
                if (response.isSuccessful) {
                    handler.post {
                        val responseBody = response.body?.string()
                        val json = JSONObject(responseBody)

                        if (json.getString("status") == "OK") {
                            val results = json.getJSONArray("results")
                            if (results.length() > 0) {
                                val firstResult = results.getJSONObject(0)
                                val formattedAddress = firstResult.getString("formatted_address")

                                callback(formattedAddress)
                            }
                        }
                    }
                }
            }
        })
    }
}