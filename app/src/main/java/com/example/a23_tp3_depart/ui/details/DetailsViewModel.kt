package com.example.a23_tp3_depart.ui.details

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.a23_tp3_depart.data.LocDatabase
import com.example.a23_tp3_depart.model.Locat

class DetailsViewModel(application: Application) : AndroidViewModel(application) {

    private lateinit var selectedLocation: LiveData<Locat?>

    private val mDb: LocDatabase = LocDatabase.getInstance(application)

    fun setSelectedLocationId(locationId: Int) {
        selectedLocation = mDb.locDao().getLocation(locationId)
    }

    fun getSelectedLocation(): LiveData<Locat?> {
        return selectedLocation
    }
}
